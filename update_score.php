<?php
include('include/common.php');
define('N', 10);

$db = new DataBase();

// 从数据库选出N个没有处理过的学号
$r = $db->get('student', 'flag = 0 order by id ASC limit '.N);
// 全部都处理过了,重置状态
if(count($r) == 0){
	$db->query('update `student` set flag = 0 where 1');
	exit;
}

// 对于每一个学生
$time = date('Y-m-d H:i:s', time());

// 挨个处理的版本
foreach ($r as $stu) {
	$db->update('student', $stu['id'], 
		array('flag'=>1, 'update_time'=>$time));
	// file_get_contents('http://localhost/hit_helper/check_new_score.php?SID='.$stu['sid']);
	check_new_score($stu['sid']);
}
/*
// 多进程的版本
foreach ($r as $stu) {
	$pid = pcntl_fork();
	if($pid == -1){
		die("fork failed!");
	}elseif ($pid) {
		pcntl_wait($status, WNOHANG);
	}else{
		//子进程
		pcntl_exec('/usr/local/bin/php', array('-v'));
		exit();
	}
}
*/

