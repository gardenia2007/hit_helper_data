<?php
include('include/common.php');

$tran = array(
	"自然班号"	=> 'CID',
	#"教学班号"	=> 'teach_class',
	'院　　系'	=> 'college',
	'专业(类)'	=> 'major',
	'录取来源'	=> 'location',
	#'录取形式'	=> 'asdf',
	'姓　　名'	=> 'name',
	#'姓名拼音(英文名)' => 'py_name',
	'性　　别'	=> 'sex',
	'民　　族'	=> 'ethnic',
	'出生日期'	=> 'birthday',
	'身份证号'	=> 'IDCard',
	'本人联系电话'=> 'phone'
	);

 if(!(isset($_GET['SID']) && isset($_GET['IDC']))){
 	echo json_encode(array('status'=>'F', 'msg'=>'参数错误', 'data'=>array()));
 }else{
	$sid = $_GET['SID'];
	$cid = $_GET['IDC'];

	$data = get_info_by_no($sid);

	if($data == FALSE){
		$status = "F";
		$msg = '没有相应的信息';
	}else
	#检查身份证号码是否对应
	if($cid == 'xxxhit' || substr($data['IDCard'], -6) == $cid){
		$status = 'S';
		$msg = '';
	}else{
		$status = 'F';
		$msg = '学号与身份证号码不对应，请检查';
		$data = array();
	}

	$ret = array(
		'status'=>	$status,
		'msg'	=>	$msg,
		'data'	=>	$data
		);

	echo json_encode($ret);

	// 初始化成绩数据
	init_score($sid);
}

