<?php
include('include/common.php');

if(!(isset($_GET['SID']) && isset($_GET['term']))){
	echo json_encode(array('status'=>'F', 'msg'=>'参数不正确'));
	exit;
}

$sid = $_GET['SID'];
$term = $_GET['term'];

$status = 'F';
$msg = '未知错误';
$data = array();

$score = get_all_score($sid);
if($score == FALSE){
	$status = 'F';
	$msg = '没有查找到任何成绩';
}else{
	$status = 'S';
	$msg = ''; 
	if($term == ''){ // 如果学期为空，返回全部学期成绩
		$data = $score;
	}else{ // 选择某个学期的成绩
		if(isset($score[$term])){
			$data[$term] = $score[$term];
		}else{
			$status = 'F';
			$msg = '没有该学期的成绩信息';
		}
	}
}


echo json_encode(array(
	'status' => $status, 
	'msg'	 => $msg,
	'data'	 => $data
	)
);


