<?php 
function fetch_page($id, $url, $post_data = NULL){
	$class = substr($id,1,-2);

	#cookie file path
	$path = dirname(__FILE__)."/cookie-zx";

	#cookie
	$cookie_string = file_get_contents($path);

	#make cookie string depend the userinfo
	# TODO sprintf
	$cookie_string2 = preg_replace('/UserName=[^;]*;/','UserName=;',$cookie_string);
	$cookie_string2 = preg_replace('/UserClass=[^;]*;/','UserClass='.$class.';',$cookie_string2);
	$cookie_string2 = preg_replace('/UserID=[^;]*;/','UserID='.$id.';',$cookie_string2);

	#curl begin
	$curl = curl_init($url);

	if($curl)
	{
		if(is_null($post_data)){
			# get 方式
			curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-type: application/x-www-form-urlencoded',
				'Cookie: '.$cookie_string2));
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);
		}else{
			# post 数据
			#$post_data = array( "selectXQ" => "all", "LB" => 1, "Submit" => "成绩查询",);
			$post_data_str = http_build_query($post_data);
			$length = strlen($post_data_str);

			curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-type: application/x-www-form-urlencoded',
				'Content-length: '.$length,'Cookie: '.$cookie_string2));
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);
			curl_setopt($curl,CURLOPT_POST,TRUE);
			curl_setopt($curl,CURLOPT_POSTFIELDS,$post_data_str);
		}
	}

	$data = curl_exec($curl);
	file_put_contents("./tmp", $data);

	return $data;


}

