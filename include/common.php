<?php

include_once('simple_html_dom.php');
include_once('config.php');
include_once('db.php');

define('SPACE', 50);


function fetch_page($id, $url, $post_data = NULL){
	$class = substr($id,1,-2);

	#cookie file path
	$path = dirname(__FILE__)."/cookie-zx";

	#cookie
	$cookie_string = file_get_contents($path);

	#make cookie string depend the userinfo
	# TODO sprintf
	$cookie_string2 = preg_replace('/UserName=[^;]*;/','UserName=;',$cookie_string);
	$cookie_string2 = preg_replace('/UserClass=[^;]*;/','UserClass='.$class.';',$cookie_string2);
	$cookie_string2 = preg_replace('/UserID=[^;]*;/','UserID='.$id.';',$cookie_string2);

	#curl begin
	$curl = curl_init($url);

	if($curl)
	{
		if(is_null($post_data)){
			# get 方式
			curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-type: application/x-www-form-urlencoded',
				'Cookie: '.$cookie_string2));
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);
		}else{
			# post 数据
			#$post_data = array( "selectXQ" => "all", "LB" => 1, "Submit" => "成绩查询",);
			$post_data_str = http_build_query($post_data);
			$length = strlen($post_data_str);

			curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-type: application/x-www-form-urlencoded',
				'Content-length: '.$length,'Cookie: '.$cookie_string2));
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);
			curl_setopt($curl,CURLOPT_POST,TRUE);
			curl_setopt($curl,CURLOPT_POSTFIELDS,$post_data_str);
		}
	}
	$data = curl_exec($curl);

	return $data;
}


// 根据学号获取全部成绩信息
function get_all_score($sid){
	global $fetch_url;
	$page = fetch_page($sid, $fetch_url['score']);

	$html = str_get_html($page);

	$tbl = $html->find('div[id=spacemain] div[class=center]', 0)->find('table', 1);
	if(is_null($tbl))
		return FALSE;

	$score = array();
	// 每一行都是一条成绩记录
	foreach ($tbl->find('tr') as $tr) {
		$tds = $tr->find('td');
		$term = $tds[0]->plaintext;
		if(substr($term, 0, 2) == '20'){ // 确实是成绩记录
			$d['cid'] = trim($tds[1]->plaintext);
			$d['cname'] = trim(str_replace('&nbsp;', '', $tds[2]->plaintext));
			$d['score'] = trim($tds[7]->plaintext);
			$d['comment'] = trim(str_replace('　', '', $tds[8]->plaintext));
			$score[$term][] = $d;
		}
	}
	return $score;
}

//根据学号获取信息
function get_info_by_no($no){
	global $tran;
	global $fetch_url;
	$page = fetch_page($no, $fetch_url['info']);

	$html = str_get_html($page);

	$form = $html->find('form', 0);

	if(is_null($form))
		return FALSE;

	$content = $form->find('table', 0);

	$data = array();

	foreach ($content->find('tr') as $tr) {
		$tds = $tr->find('td');
		for ($i=0; $i < 4; $i+=2) {
			if(!isset($tds[$i])) continue;
			$td = trim($tds[$i]->plaintext);
			// $td = str_replace("\t", '', $td);
			if(isset($tran[$td])){
				$data[$tran[$td]] = str_replace('　', '', trim($tds[$i+1]->plaintext));
			}
		}
	}

	if(count($data) == 0)
		return FALSE;
	
	$phone = $content->find('input[id=ssdh]', 0);
	$data['phone'] = $phone->value;
	$data['SID'] = $no;

	return $data;
}

// 初始化成绩数据到数据库中
function init_score($sid){
	$score = get_all_score($sid);
	// var_dump($score);
	$db = new DataBase();

	if($db->count('student', 'sid = '.$sid) == 0){
		// 保存用户信息
		$db->insert('student', array('sid'=>$sid));

		$order = 0;
		// 把所有成绩都保存到数据库
		foreach ($score as $k => $term) {
			foreach ($term as $item) {
				$item['term'] = $k;
				$item['sid'] = $sid;
				// 为后加入的成绩数据留下空间，SPACE
				$item['order'] = ($order++) * SPACE;
				$db->insert('score', $item);
			}
		}
	}
}


function check_new_score($sid)
{
	global $push_url;
	$db = new DataBase();

	$old_count = $db->count('score',  'sid = '.$sid);
	$now_score = get_all_score($sid);
	$now_count = 0;
	foreach ($now_score as $term => $k) {
		$now_count += count($k);
	}

	if($now_count >= $old_count){
		// 成绩有更新
		$old_score = $db->get('score', 'sid='.$sid.' order by `order` ASC, `id` ASC');
		$max_order = $db->max('score', 'order', '`sid` = '.$sid) + SPACE;
		// var_dump($max_order);
		$o_i = 0;
		$new_score = array();
		// 这样做的前提是成绩的顺序是不变的，针对补考成绩特殊考虑，数据库中放置order字段
		foreach ($now_score as $term => $k) {
			foreach ($k as $item) {
				if($o_i >= $old_count){
					// 原来的成绩都扫描一编了，下面的都是新出现的成绩
					$item['order'] = $max_order;
					$max_order += SPACE;
					$new_score[$term][] = $item;
				}
				// 如果满足下面两个条件，就认为两条记录是一样的
				elseif($old_score[$o_i]['cid'] == $item['cid'] && $old_score[$o_i]['score'] == $item['score']){
					$o_i ++;
					continue;
				}else{
					//记录不匹配，说明有更新
					$item['order'] = $old_score[$o_i]['order'] - 1;
					$new_score[$term][] = $item;
				}
			}
		}

		var_dump($new_score);
		// 推送消息
		$json = json_encode(array(
			'status'=> 'S',
			'msg'	=> '',
			'data'	=> $new_score
			));

		$url = $push_url."?SID=$sid&data=$json";
		// $url = "http://localhost/hit_helper/echo.php?SID=$sid&json=$json";
		$ret = file_get_contents($url);

		// 把所有更新的成绩都保存到数据库
		foreach ($new_score as $k => $term) {
			foreach ($term as $item) {
				$item['term'] = $k;
				$item['sid'] = $sid;
				echo $db->insert('score', $item);
			}
		}
	}
}



